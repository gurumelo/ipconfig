Servidor:

- Se entra en ISPConfig.

- Se crea un registro DNS de tipo A, por ejemplo,
home.servidor.es o lo que sea

- mysql, use dbispconfig, SELECT * FROM dns_rr. Nos apuntamos el id que
tiene el nuevo registro DNS

- Se crea un usuario remoto en ISPconfig.

-- Sistema → Usuarios remotos → Añadir usuario

-- Nombre, contraseña, [marcado] remote access, Remote Access IPs /
Hostname: la ip del propio servidor (ispconfig), para que sólo se pueda ejecutar
desde ahí

-- Permisos: 

---Funciones para Zona DNS
---Funciones DNS para registro A




- El directorio servidor se pone en algún directorio accesible via web
(php), por ejemplo: ip.servidor.es/ipconfig/

- nano actualizandoip.php

-- Sustituir las variables

--- usuario y contrasena: son dos variables inventadas, las usará el
cliente para poder hacer la petición

--- iddns: el id del registro DNS A que se creó en los primeros pasos

--- username y password: correspondientes al usuario remoto creado en
ispconfig

--- apiurl: sustituir por lo que corresponda, donde está el ispconfig.


y ya está, luego la parte cliente sería ponerla a funcionar en un cron. Básicamente comprueba cada X la ip externa, si es
igual a la que tuviese no hace nada, si es diferente, actualiza la ip en
el servidor, si es la primera vez que se ejecuta, crea ip.txt y actualiza.
