#!/bin/bash
# Sustituir url por la que corresponda 
url="https://url.es"
u="XXXXa"
p="XXXXb"

ip=$( wget -O - -q "${url}/ipconfig/cualeslaip.php" )

if [ ! -f "ip.txt" ]
then
	echo $ip > ip.txt
	api="${url}/ipconfig/actualizandoip.php?usuario=${u}&contrasena=${p}&ip=${ip}"
	wget -O - -q $api
else
	ipantigua=$( cat ip.txt )
	if  [ "${ipantigua}" != "${ip}" ]
	then
		echo $ip > ip.txt
		api="${url}/ipconfig/actualizandoip.php?usuario=${u}&contrasena=${p}&ip=${ip}"
		wget -O - -q $api
	fi
fi
